 <?php
include ('koneksi.php');
session_start();
if (!isset($_SESSION['username'])){
	header("location:login.php");
	}
error_reporting(0);
?>
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Sufee Admin - HTML5 Admin Template</title>
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="favicon.ico">

    <link rel="stylesheet" href="vendors/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="vendors/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="vendors/themify-icons/css/themify-icons.css">
    <link rel="stylesheet" href="vendors/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="vendors/selectFX/css/cs-skin-elastic.css">
    <link rel="stylesheet" href="vendors/jqvmap/dist/jqvmap.min.css">


    <link rel="stylesheet" href="assets/css/style.css">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

</head>

<body>
<?php
	if ($_SESSION['id_level']==1){			
?>

    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">

            <div class="navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="./"><img src="images/logo.png" alt="Logo"></a>
                <a class="navbar-brand hidden" href="./"><img src="images/logo2.png" alt="Logo"></a>
            </div>

            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
					<li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-tasks"></i>Dashboard</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-fort-awesome"></i><a href="index.php">Dashboard</a></li>
                            <li><i class="menu-icon ti-themify-logo"></i><a href="index1.php">Data</a></li>
                        </ul>
                    </li>
                    <h3 class="menu-title">Menu Utama</h3><!-- /.menu-title -->
                    <li>
                        <a href="inventaris.php"> <i class="menu-icon fa fa-laptop"></i>Inventaris</a>
					</li>
					<li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-tasks"></i>Peminjaman</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-fort-awesome"></i><a href="tambah_pinjam.php">Pinjam</a></li>
                            <li><i class="menu-icon ti-themify-logo"></i><a href="tampil_pinjam.php">Data</a></li>
                        </ul>
                    </li>
					<li>
                        <a href="pengembalian.php"> <i class="menu-icon fa fa-th"></i>Pengembalian</a> 
                    </li><li>
                        <a href="laporan.php"> <i class="menu-icon fa fa-th"></i>Laporan</a> 
                    </li>		

                    <h3 class="menu-title">Lainnya</h3><!-- /.menu-title -->

                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-tasks"></i>Master Data</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon ti-themify-logo"></i><a href="ruang.php">Ruangan</a></li>
                            <li><i class="menu-icon ti-themify-logo"></i><a href="jenis.php">Jenis</a></li>
                            <li><i class="menu-icon ti-themify-logo"></i><a href="petugas.php">Petugas</a></li>
                            <li><i class="menu-icon ti-themify-logo"></i><a href="pegawai.php">Pegawai</a></li>
                        </ul>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside><!-- /#left-panel -->
<!-- /#left-panel -->

    <!-- Left Panel -->
<?php }
	elseif ($_SESSION['id_level']==2){ 
?>

    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">

            
            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="active">
                        <a href="index.php"> <i class="menu-icon fa fa-dashboard"></i>Dashboard </a>
                    </li>
                    <h3 class="menu-title">Menu Utama</h3><!-- /.menu-title -->
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-tasks"></i>Peminjaman</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-fort-awesome"></i><a href="tambah_pinjam.php">Pinjam</a></li>
                            <li><i class="menu-icon ti-themify-logo"></i><a href="tampil_pinjam.php">Data</a></li>
                        </ul>
                    </li>
					<li>
                        <a href="pengembalian.php"> <i class="menu-icon fa fa-th"></i>Pengembalian</a> 
                    </li>		
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside><!-- /#left-panel -->
<?php } 
	elseif ($_SESSION['id_level']==3){
?>

    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">

            <div class="navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="./"><img src="images/logo.png" alt="Logo"></a>
                <a class="navbar-brand hidden" href="./"><img src="images/logo2.png" alt="Logo"></a>
            </div>

            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="active">
                        <a href="index.php"> <i class="menu-icon fa fa-dashboard"></i>Dashboard </a>
                    </li>
                    <h3 class="menu-title">Menu Utama</h3><!-- /.menu-title -->
    				<li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-tasks"></i>Peminjaman</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-fort-awesome"></i><a href="tambah_pinjam.php">Pinjam</a></li>
                            <li><i class="menu-icon ti-themify-logo"></i><a href="tampil_pinjam.php">Data</a></li>
                        </ul>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside><!-- /#left-panel -->
	<?php } ?>

    <div id="right-panel" class="right-panel">
        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Dashboard</h1>
                    <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>
					</div>
                </div>
            </div>
            <div class="col-sm-8" >
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li class="menu-item-has-children dropdown">
								<a href="logout.php" class="dropdown-toggle"> <i class="fa fa-gears"></i>			 Logout</a>
							</li>
                        </ol>
					</div>
                </div>
            </div>
        </div>

        <?php
	if ($_SESSION['id_level']==1){			
?>

<div class="content mt-3">
			<div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">

                    <div class="col-md-128">
                        <div class="card">
                            
                            <div class="card-body">
                                <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
												<th class="column-title">ID Peminjaman </th>
                                                <th class="column-title">Tanggal Pemijaman </th>
                                                <th class="column-title">Jumlah </th>
                                                <th class="column-title">Status Peminjaman </th>
                                                <th class="column-title">ID Pegawai </th>
                                                <th class="column-title no-link last"><span class="nobr">Action</span>
                                                </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
												include "koneksi.php";
												$no=1;
												$select=mysqli_query($conn,"SELECT a.`id_peminjaman`, a.`tgl_pinjam`, a.`jumlah`, a.`stts_pinjam`, a.`id_pegawai`, a.`id_inventaris`
																			FROM `peminjaman` a 
																			left join inventaris b on a.id_inventaris=b.id_inventaris");
												
												while($data=mysqli_fetch_array($select))
												{
										?>
												<tr>
													<td><?php echo $data['id_peminjaman'];?></td>
													<td><?php echo $data['tgl_pinjam'];?></td>
													<td><?php echo $data['jumlah'];?></td>
													<td><?php echo $data['stts_pinjam'];?></td>
													<td><?php echo $data['id_pegawai'];?></td>
												
                                                    <td><a class="btn outline btn-white fa fa-edit" href="edit_pinjam.php"></a>
														<a class="" href="detail_pinjam.php">Detail</a></td>
												<?php
												} 
												?>
                                    </tbody>
                                </table>
								<td><a  href="tambah_pinjam.php" type="button" class="btn btn-primary">Pinjam</button></a></td>
								<td><a  href="view.php" type="button" class="btn btn-primary">AAAAAAA</button></a></td>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

<?php }
	elseif ($_SESSION['id_level']==2){ 
?>

<div class="content mt-3">
			<div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">

                    <div class="col-md-128">
                        <div class="card">
                            
                            <div class="card-body">
                                <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
												<th class="column-title">ID Peminjaman </th>
                                                <th class="column-title">Tanggal Pemijaman </th>
                                                <th class="column-title">Jumlah </th>
                                                <th class="column-title">Status Peminjaman </th>
                                                <th class="column-title">ID Pegawai </th>
                                                <th class="column-title no-link last"><span class="nobr">Action</span>
                                                </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
												include "koneksi.php";
												$no=1;
												$select=mysqli_query($conn,"SELECT a.`id_peminjaman`, a.`tgl_pinjam`, a.`jumlah`, a.`stts_pinjam`, a.`id_pegawai`, a.`id_inventaris`
																			FROM `peminjaman` a 
																			left join inventaris b on a.id_inventaris=b.id_inventaris");
												
												while($data=mysqli_fetch_array($select))
												{
										?>
												<tr>
													<td><?php echo $data['id_peminjaman'];?></td>
													<td><?php echo $data['tgl_pinjam'];?></td>
													<td><?php echo $data['jumlah'];?></td>
													<td><?php echo $data['stts_pinjam'];?></td>
													<td><?php echo $data['id_pegawai'];?></td>
												
                                                    <td><a class="btn outline btn-white fa fa-edit" href="edit_pinjam.php"></a>
														<a class="" href="detail_pinjam.php">Detail</a></td>
												<?php
												} 
												?>
                                    </tbody>
                                </table>
								<td><a  href="tambah_pinjam.php" type="button" class="btn btn-primary">Pinjam</button></a></td>
								<td><a  href="view.php" type="button" class="btn btn-primary">AAAAAAA</button></a></td>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

<?php } 
	elseif ($_SESSION['id_level']==3){
?>

<div class="content mt-3">
			<div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">

                    <div class="col-md-128">
                        <div class="card">
                            
                            <div class="card-body">
                                <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
												<th class="column-title">ID Peminjaman </th>
                                                <th class="column-title">Tanggal Pemijaman </th>
                                                <th class="column-title">Jumlah </th>
                                                <th class="column-title">Status Peminjaman </th>
                                                <th class="column-title">ID Pegawai </th>
                                                </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
												include "koneksi.php";
												$no=1;
												$select=mysqli_query($conn,"SELECT a.`id_peminjaman`, a.`tgl_pinjam`, a.`jumlah`, a.`stts_pinjam`, a.`id_pegawai`, a.`id_inventaris`
																			FROM `peminjaman` a 
																			left join inventaris b on a.id_inventaris=b.id_inventaris");
												
												while($data=mysqli_fetch_array($select))
												{
										?>
												<tr>
													<td><?php echo $data['id_peminjaman'];?></td>
													<td><?php echo $data['tgl_pinjam'];?></td>
													<td><?php echo $data['jumlah'];?></td>
													<td><?php echo $data['stts_pinjam'];?></td>
													<td><?php echo $data['id_pegawai'];?></td>
												
                                                 
												<?php
												} 
												?>
                                    </tbody>
                                </table>
								<td><a  href="tambah_pinjam.php" type="button" class="btn btn-primary">Pinjam</button></a></td>
								<td><a  href="view.php" type="button" class="btn btn-primary">AAAAAAA</button></a></td>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

<?php } ?>

    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <script src="vendors/popper.js/dist/umd/popper.min.js"></script>
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="assets/js/main.js"></script>


    <script src="vendors/chart.js/dist/Chart.bundle.min.js"></script>
    <script src="assets/js/dashboard.js"></script>
    <script src="assets/js/widgets.js"></script>
    <script src="vendors/jqvmap/dist/jquery.vmap.min.js"></script>
    <script src="vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <script src="vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script>
        (function($) {
            "use strict";

            jQuery('#vmap').vectorMap({
                map: 'world_en',
                backgroundColor: null,
                color: '#ffffff',
                hoverOpacity: 0.7,
                selectedColor: '#1de9b6',
                enableZoom: true,
                showTooltip: true,
                values: sample_data,
                scaleColors: ['#1de9b6', '#03a9f5'],
                normalizeFunction: 'polynomial'
            });
        })(jQuery);
    </script>

</body>

</html>
